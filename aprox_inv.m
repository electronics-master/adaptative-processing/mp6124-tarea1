function V_q = aprox_inv (A, tol)
  # Devuelve la inversa derecha de A

  # A only has right inverse
  if(columns(A)<rows(A))
    printf("A has no right inverse\n")
    return
  endif
  
  # Primera aproximación, tomada del ejemplo 4.1
  V_q = transpose(A)./(norm(A)^2);

  while (frobenius(A*V_q-eye(size(A*V_q))) > tol)
    V_q = V_q * ( 3*eye(size(A*V_q)) - 3*A*V_q + (A*V_q)^2 );
  endwhile
endfunction
