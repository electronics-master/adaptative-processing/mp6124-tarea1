function [K, e_k] = solucion_problA(m, rho, covariance, tol)
  # m: Dimension for the random vector x
  # rho: Parameter for the covariance between 0 and 1
  # covariance: Covariance for the n vector diagonal
  # tol tolerance for the inverse matrix

  R_xx = (rho*ones(m, m) - (rho-1)*eye(m, m));

  R_vv = covariance*(eye(m, m));

  A = full(gallery("tridiag", m, 1, 3, 1));
  # The transpose for A is equal to A

  R_xy = R_xx*A;

  R_yy = A*R_xx*A+R_vv;
  
  K = R_xy*aprox_inv(R_yy, tol);

  e_k = trace(R_xx - K*R_yy*transpose(conj(K)));
endfunction
