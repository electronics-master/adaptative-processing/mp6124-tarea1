function norm = frobenius (A)
  norm = sum(sum(A.*A));
endfunction
