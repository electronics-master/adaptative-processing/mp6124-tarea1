
# Varying value for m2-50
m = [2:1:50];
rho = ones(size(m))*0.5;
sig = ones(size(m))*1;
tol = ones(size(m))*1e-4;

[K, e_k] = arrayfun(@solucion_problA, m, rho, sig, tol, "UniformOutput", false);

h = plot(m, cell2mat(e_k));
waitfor(h);

# Varying values of sigma 0.1-5
sig = [0.1:0.1:5];
m = ones(size(sig))*25;
rho = ones(size(sig))*0.5;
tol = ones(size(sig))*1e-4;

[K, e_k] = arrayfun(@solucion_problA, m, rho, sig, tol, "UniformOutput", false);

h = plot(sig, cell2mat(e_k));
waitfor(h);

# Varying values of rho 0-99
rho = [0:1:99];
sig = ones(size(rho))*1;
m = ones(size(rho))*25;
tol = ones(size(rho))*1e-4;

[K, e_k] = arrayfun(@solucion_problA, m, rho, sig, tol, "UniformOutput", false);

h = plot(rho, cell2mat(e_k));
waitfor(h);
